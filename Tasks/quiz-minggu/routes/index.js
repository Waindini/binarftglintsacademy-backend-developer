const { Router} = require('express');
const bookListController = require('../controller/booklist');
const bookListRouter = require('./booklist');

const router = Router();

//get, post, put, delete
router.get('/', (req,res) => {
    //res.send("HELLO THERE, WELCOME!");
    res.render("index.ejs");
});
router.get('/books/add', (req, res) => {
    res.send("Feel Free to Add the Book!");
})
router.post('/books/add', (req, res) => {
    res.send("Please, Insert on this!");
})
router.get('/books/edit/:id', (req, res) => {
    res.send("Form to Edit the Book");
})
router.post('/books/edit/:id', (req, res) =>{
    res.send("Update Book by Id");
})
router.get('/books/delete/:id', (req, res) => {
    res.send("Delete Book by Id");
})

router.use('/booklist', bookListRouter);

module.exports = router;