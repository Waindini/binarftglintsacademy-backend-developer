const { Router } = require('express');
const bookListController = require('../controller/booklist');
const router = Router();


router.get('/', bookListController.getBookList)
router.get('/add', bookListController.addFormBookList)
router.post('/add', bookListController.addBookList)
router.get('/edit/:id', bookListController.updateBookList)
router.post('/edit/:id', bookListController.updateBookList)
router.get('/delete/:id', bookListController.deleteBookList)
router.get('/:id', bookListController.findById)
module.exports = router;