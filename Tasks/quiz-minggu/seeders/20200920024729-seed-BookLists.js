'use strict';
const fs = require('fs');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   const parseData = JSON.parse(fs.readFileSync('./books.json'));
   const bookListsData = [];
   parseData.forEach(data => {
     const { title, author, pages, genre } = data;
     bookListsData.push({
       title,
       author, 
       pages, 
       released_date : new Date(),
       pages,
       genre
     })
   })
   await queryInterface.bulkInsert('BookLists', bookListsData, {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('BookLists', null, {});
  }
};

