const { booklist } = require('../models')

class bookListController {
    static getBookList(req, res) {
        BookList.findAll()
            .then(result => {
                // console.log(result);
                res.send(result)
                //res.render('index.ejs', { booklist: result })
            })
            .catch(err => {
                console.log(err);
            })
    }
    static addFormBookList(req, res) {
        res.render('addBookList.ejs');
    }
    static addBookList(req, res) {
        const { title, author, pages, genre } = req.body;
        BookList.create({
            title,
            author,
            pages,
            genre
        })
            .then(result => {
                // res.send(result)
                res.redirect('/booklist')
            })
            .catch(err => {
                res.send(err)
            })
    }

    static findById(req, res) {
        const id = req.params.id;
        BookList.findOne({
            where: { id }
        })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }

    static deleteBookList(req, res) {
        const id = req.params.id;
        BookList.destroy({
            where: { id }
        })
            .then(() => {
                // res.send("Deleted")
                res.redirect('/booklist')
            })
            .catch(err => {
                res.send(err)
            })
    }

    static updateBookList(req, res) {
        const id = req.params.id;
        const { title, author, pages, genre } = req.body;
        BookList.update({
            title,
            author,
            pages,
            genre
        }, {
            where: { id }
        })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }
}

module.exports = bookListController; 