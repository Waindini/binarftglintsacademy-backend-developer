const { Ship } = require('../models')

class ShipController {
    static getShip(req, res) {
        Ship.findAll({
            order : [
                ['id', 'ASC']
            ]
        })
            .then(result => {
                res.json(result);
                // res.render('ships.ejs', { ships: result })
            })
            .catch(err => {
                console.log(err);
            })
    }
    static addFormShip(req, res) {
        res.render('addShips.ejs');
    }
    static addShip(req, res) {
        const { name, info, image, hammock } = req.body;

        Ship.findOne({
            where : {
                name
            }
        })
        .then(found => {
            if(found){
                res.send("Name already exist! Try another name arigato.")
            }else {
                return Ship.create({
                    name,
                    info,
                    image,
                    hammock
                })
            }
        }) 
        .then(result => {
            res.send(result)
            // res.redirect('/ships')
        })
        .catch(err => {
            res.send(err)
        })
           
    }

    static findById(req, res) {
        const id = req.params.id;
        Ship.findOne({
            where: { id }
        })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }

    static deleteShip(req, res) {
        const id = req.params.id;
        Ship.destroy({
            where: { id }
        })
            .then(() => {
                // res.send("Deleted")
                res.redirect('/ships')
            })
            .catch(err => {
                res.send(err)
            })
    }

    static editFormShip(req, res) {
        const id = req.params.id;
        // console.log(id)
        Ship.findOne({
            where : { id }
        })
        .then(result => {
            console.log(result)
            res.render('editShips.ejs', { ship:result });
        })
        .catch(err=>{
            res.send(err);
        })
    }

    static editShip(req, res) {
        const id = req.params.id;
        const { name, info, image, hammock } = req.body;
        Ship.update({
            name,
            info,
            image,
            hammock
        }, {
            where: { id }
        })
            .then(result => {
                if(result[0] === 1) {
                    res.redirect('/ships')
                }else{
                    res.send('Update not done!')
                }
                // res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }
}

module.exports = ShipController; 
