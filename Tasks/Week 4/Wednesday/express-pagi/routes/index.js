const { Router} = require('express');
const router = Router();

router.get('/', (req,res) => {
    // res.send("HOME PAGE");
    res.render("Product List");
})

module.exports = router;