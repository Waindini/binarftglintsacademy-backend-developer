const { Pool } = require('pg');
const pool = new Pool({
    database: 'pg_basic',
    host: 'localhost',
    user: 'postgres',
    password: '123qwertyA',
    port: 5432
});

module.exports = pool; 
