const { TodoLists } = require('../models')

class TodoListsController {
    static getTodoLists(req, res) {
        // console.log("Coba check")
        TodoLists.findAll()
            .then(result => {
                // console.log(result);
                //res.send(result)
                // console.log({todolists: result});
                res.render('todolist.ejs', { todolists: result }) //todolists jd key
            })
            .catch(err => {
                console.log(err);
            })
    }
    static addFormTodoLists(req, res) {
        res.render('addTodoList.ejs');
    }
    static addTodoLists(req, res) {
        const { name, type, status } = req.body;
        TodoLists.create({
            name,
            type,
            status
        })
            .then(result => {
                // res.send(result)
                res.redirect('/todoList')
            })
            .catch(err => {
                res.send(err)
            })
    }

    static findById(req, res) {
        const id = req.params.id;
        // const id = 1;
        TodoLists.findOne({
            where: { id }
        })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }

    static deleteTodoLists(req, res) {
        const id = req.params.id;
        console.log(req.params.id);
        TodoLists.destroy({
            where: { id }
        })
            .then(() => {
                // res.send("Deleted")
                res.redirect('/todolist')
            })
            .catch(err => {
                res.send(err)
            })
    }

    static updateTodoLists(req, res) {
        const id = req.params.id;
        const { name, type, status } = req.body;
        TodoLists.update({
            name,
            type,
            status
        }, {
            where: { id }
        })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }
}

module.exports = TodoListsController; 
