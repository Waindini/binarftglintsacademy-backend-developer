'use strict';
const fs = require('fs');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   const parseData = JSON.parse(fs.readFileSync('./data.json'));
   const todoListsData = [];
   parseData.forEach(data => {
     const { name, type, status } = data;
     todoListsData.push({
       name,
       type, 
       status, 
       createdAt : new Date(),
       updatedAt : new Date()
     })
   })
   await queryInterface.bulkInsert('TodoLists', todoListsData, {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('TodoLists', null, {});
  }
};

 