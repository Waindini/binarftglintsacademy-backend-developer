const { Router} = require('express');
const todoListController = require('../controller/todolist');
const TodoListRouter = require('./todolist');

const router = Router();

router.get('/', (req,res) => {
    //res.send("HELLO THERE, WELCOME!");
    res.render("index.ejs");
});
router.use('/todolist', TodoListRouter);

module.exports = router;