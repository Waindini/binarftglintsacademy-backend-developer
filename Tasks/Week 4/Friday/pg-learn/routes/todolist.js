const { Router } = require('express');
const todoListController = require('../controller/todolist');
const router = Router();


router.get('/', todoListController.getTodoLists)
router.get('/add', todoListController.addFormTodoLists)
router.post('/add', todoListController.addTodoLists)
router.get('/delete/:id', todoListController.deleteTodoLists)
router.get('/edit/:id', todoListController.updateTodoLists)
router.get('/:id', todoListController.findById)

module.exports = router;
