// node todo.js help //menampilkan commandd apa saja yang tersedia
// node todo.js list //Melihat daftar TODO
// node todo.js add <content> //Menambahkan TODO ke dalam list
// node todo.js update <id> <task>//Melihat detail TODO sesuai `id` nya
// node todo.js delete <id> //Menghapus TODO sesuai `id` nya
// node todo.js complete <id> //Menandai status TODO selesai
// node todo.js uncomplete <id> //Menandai status TODO belum selesai

const command = process.argv[2];
const params = process.argv.slice(3);
const TodoController = require('./controller/TodoController')

switch (command) {
case 'showToDo':
    TodoController.showToDo();     
    break;
case 'addToDo':
    TodoController.addToDo();
    break;
case 'updateTodoListByID':
    TodoController.updateTodoList();
    break;
case 'deleteTodoList':
    TodoController.deleteTodoList();
    break;
case 'completeTodoList':
    TodoController.completeTodoList();
    break;
case 'uncomplete':
    TodoController.uncompleteTodoList();
    break;
case 'giveHelp':
    TodoController.help();
    break;
}