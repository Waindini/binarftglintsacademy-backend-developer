const TodoList = require("../Model/TodoList");
const ToDo = require("../Model/ToDo");
// class TodoUser

class TodoUser {
    static addToDoToTodoList(ToDo) {
        if (!ToDo.length) {
          return console.error("Good to see you!");
        }
    
        if (ToDo.length > 10) {
          return console.error("Max ToDo is 10");
        }
    
        for (let i = 0; i < ToDo.length; i++) {
          const { name, type } = ToDo[i];
          const newTodo = new Todo (name, type);
          TodoList.addToDo(newTodo);
        }
      }
    
      static todoFromTodoList(filter) {
        const ToDo = TodoList.listTodo(filter);
    
        return ToDo;
      }
    
      static deleteTodoList(ids) {
        const ToDo = TodoList.listTodo();
    
        for (let i = 0; i < ids.length; i++) {
          // ini untuk menemukan Todo didalam ToDo, sesuai ID yaa harusnya
          const selectedTodo = ToDo.find(b => b.id === ids[i]);
    
          if (selectedTodo) {
            TodoList.deleteTodo(ids[i]);
          }
        }
      }
    
      static updateTodoToTodoList(id, field, value) {
        let ToDo = TodoList.listTodo();
        // ini untuk menemukan Todo didalam ToDo, sesuai ID
        const selectedTodo = ToDo.find(b => b.id === id);
        if (selectedToDo) {
          // yang ini untuk menemukan posisi Todo (posisi Todo di dalam Array ToDo), yang nantinya dipake untuk update Array ToDo dengan benar
          // coba gini ya
          // ini coba2 contoh logic yg dipake
          // var arr = [
          //   1,
          //   2,
          //   3
          // ]
          // update to = [
          //   1,
          //   3,
          //   3
          // ]
          // how to update:
          // index = 1
          // arr[index] = 3
          const selectedTodoIndex = ToDo.findIndex(
            b => b.id === selectedTodo.id
          );
    
          if (field === "name") {
            selectedTodo.updateName(value);
          } else if (field === "type") {
            selectedTodo.updateType(value);
          } else if (field === "status") {
            selectedTodo.updateStatus(value);
          }
    
          ToDo[selectedTodoIndex] = selectedTodo;
        }
    
        TodoList.updateListTodo(ToDo);
      }
    }
    
    module.exports = TodoUser;

    // hmm gagal. hmm kuy nyoba again
    // haa gagal sedii
    // aaa todotodotodotodo
