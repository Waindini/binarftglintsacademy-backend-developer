class TodoList {
    static #listTodo = [];
  
    static addToDo(Todo) {
      this.#listToDo.push(Todo);
    }
  
    static deleteToDo(id) {
      this.#listToDo = this.#listToDo.filter(b => b.id !== id);
    }
  
    static listTodo(filterType) {
      return this.#listToDo.filter(Todo =>
        filterType ? Todo.type === filterType : Todo
      );
    }
  
    // jika ada perubahan di TodoUser, TodoList harus aware..
    static updateListToDo(list) {
      this.#listToDo = list;
    }
  }
  
  module.exports = TodoList;