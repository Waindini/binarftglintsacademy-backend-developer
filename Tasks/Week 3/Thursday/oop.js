class Student {
    constructor (name, age, dateOfBirth, gender, studentId){
        this._name = name;
        this._age = age;
        this._dateOfBirth = dateOfBirth;
        this._gender = gender;
        this.studentId = studentId;
        this.hobbies = [];
    }
  
    set Name (name){
        this._name = name; //= `Nama Murid : ${name}.`;
    }
  
    set Age (age){
        this._age = age; //= `Usia Murid : ${age} tahun.`;
    }
  
    set DateOfBirth (dateOfBirth){
        this._dateOfBirth = dateOfBirth;//= `TTL : ${dateOfBirth}.`;
    }
  
    set Gender (gender){
      if(gender === 'Male' || gender === 'Female'){
        this._gender = gender;
      } else {
        this._gender = 'Masukkan gender yang benar woyt!';
      }
    }
  
    addId (id){
      this.studentId = id; //= `id card : ${id}`;
    }
  
    addHobby (hobbies){
      this.hobbies.push(hobbies);
    }
  
    removeHobby (hobbies){
      //console.log(this.hobbies.length)
      //console.log(this.hobbies[1]);
      let remove = [];
      for(let i=0; i<=this.hobbies.length; i++){
        if(this.hobbies[i] !== hobbies){
          remove.push(this.hobbies[i]);
        } else {
          continue;
        }
      }
      this.hobbies = [];
      this.hobbies.push(remove);
      //console.log(remove);
    }
  
    get Data (){
      return `Nama : ${this._name}, 
      Umur : ${this._age} tahun, 
      TTL : ${this._dateOfBirth},
      Gender : ${this._gender},
      No Id : ${this.studentId},
      Hobby : ${this.hobbies}.`
    }
  
  
  } 
  
  const Kafka = new Student();
  Kafka.Name = 'Kafka On The Shore'; 
  Kafka.Age = '105';
  Kafka.DateOfBirth = '09 September 2001';
  Kafka.Gender = 'Male';
  
  Kafka.addId('kafka0911');
  
  Kafka.addHobby('melamun');
  Kafka.addHobby('ngedrakor');
  Kafka.addHobby('memancing perkara');
  console.log(Kafka);
  console.log(Kafka.Data);
  
  Kafka.removeHobby('melamun');
  console.log(Kafka);
  console.log(Kafka.Data);

  
