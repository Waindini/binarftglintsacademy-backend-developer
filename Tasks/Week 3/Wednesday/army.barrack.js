// **Prepare the armies and barrack which can recruit and then attacc**

// ### Ready the army! - Task 1 (15%)
// Make Army class that have attributes :
// - name
// - type
// - level (default 1)
// And method : 
// - talk (console log about his name type level)
// - training (can level army up to 10)
// Make Army class that have 
// - attributes :
//     - name
//     - type
//     - level (default 1)

// - method : 
//     - talk (console log about his name type level)
//     - training (can level army up to 10)

// ### Train another army - Task 2 (15%)
// Make another class from the Army
// - Knight
// - Spearman
// - Archer

// Each class has override the talk method.

// ### Barrack - Task 3 (50%)
// Barrack require :
// - Propertie 
//     - Array of Instance of (_not just object_)
//     ```javascript
//     [
//         Knight {
//             ...
//         }
//     ]
//     ```
// - Method
//     - Recruit is for adding in the armies slot
//     - disband is for deleting an army from the slot

// ### Group armies - Task 5 (10%)
// Insert a method to group from their type.
// ```javascript
// Result : 
// {
//     knight : [
//         ...
//     ]
// }

// ```
// ### Tidy Up files! - Task 5 (10%)
// Split the files into 3 js'es.
// - index (main) -> node index.js
