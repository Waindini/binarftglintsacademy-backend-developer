class Army {
    constructor (name, type, level){
        this._name = name;
        this._type = type;
        this._level = level || 1;
    }

//getter
get name() {
    return this._name;
}
get type() {
    return this._type;
}
get level() {
    return this._level;
}

//setter
set setName(nameSet) {
    this._name = nameSet;
}
set setType(typeSet) {
    this._type = typeSet;
}
set setLevel(levelSet){
    this._level = levelSet;
}

//method
talk (){
    console.log(`Hello there, ${this._name} in the house yo! I am ${this._type}`)
}
training (){
    this._level += 10;
}
}

//sub class from Army class
class Knight extends Army {
    constructor (name, type, level){
        super(name, type, level) //buat ngakses parents params
    }
    talk (){
        super.talk()
        console.log(`I am ${this._name} and done with ${this._level}`) //overrading method
    }
}
class Spiderman extends Army {
    constructor (name, type, level){
        super(name, type, level)
    }
    talk (){
        super.talk()
        console.log(`I am ${this._name} and done with ${this._level}`)
    }
}
class Archer extends Army {
    constructor (name, type, level){
        super(name, type, level)
    }
    talk (){
        super.talk()
        console.log(`I am ${this._name} and done with ${this._level}`)
    }
}

//Instance Of Class : bikin objek berdasarkan kelas yg telah dibuat sebelumnya
const hero = new Knight ("Roberto", "Knight", 1);
console.log(hero);
hero.talk();
hero.training();
console.log(hero);
const hero2 = new Spiderman ("Peter", "Spiderman", 1);
hero2.talk();
hero2.training();
console.log(hero2);
const hero3 = new Archer ("Arson", "Archer", 1);
hero3.talk();
hero3.training();
console.log(hero3);

module.exports = {
     Knight,
     Spiderman,
     Archer
 }
