/**
* Seorang farmer memiliki sebuah Kebun. Dalam kebun tersebut terdapat banyak pohon buah. Seperti Apple, Pear, Watermelon. dsb
* Setiap pohon tersebut dapat Bertumbuh, Menghasilkan buah dalam usia matang nya, dan berhenti untuk di Budidayakan pada umur tertentu.
* 
* Buat sebuah class FruitTree, yang memiliki attribute :
* - type 
* - age
* - fruits
* - totalFruits
* - matureAge -> pohon akan menghasilkan buah
* - stopProducing 
* Dan memiliki method : 
* - growUp()
* - produceFruits()
* - status()
*/



class FruitTree {
    constructor(type, age, fruits, totalFruits, matureAge, stopProducing) {
        this._type = type;
        this._age = age;
        this._fruits = fruits || 0;
        this._totalFruits = totalFruits || 0;
        this._matureAge = matureAge;
        this._stopProducing = stopProducing;
    }
    //Getter
    get type() {
        return this._type;
    }
    get age() {
        return this._age;
    }
    get fruits() {
        return this._fruits;
    }
    get totalFruits() {
        return this._totalFruits;
    }
    get matureAge() {
        return this._matureAge;
    }
    get stopProducing() {
        return this._stopProducing;
    }
    //Setter
    set setType(type) {
        this._type = type;
    }
    set setAge(age) {
        this._age = age;
    }
    set setFruits(fruit) {
        this._fruits = fruit;
    }
    set setTotalFruits(fruit) {
        this._totalFruits = fruit;
    }
    set setMatureAge(age) {
        this._matureAge = age;
    }
    set setStopProducing(age) {
        this._stopProducing = age;
    }

    growUp() {
        let randomAge = Math.ceil(Math.random() * 3);
        this.setAge = this.age + randomAge;
    }
    produceFruits() {
        let fruitLimit = 15;
        let fruitProduced = Math.ceil(Math.random() * fruitLimit);

        if (this.age >= this.matureAge && this.age <= this.stopProducing) {
            this.setFruits = fruitProduced;
            this.setTotalFruits = this.totalFruits + fruitProduced;
        }
    }
    status() {
        if (this.age < this.stopProducing) {
            console.log(`${this.type} tree is ${this.age} years old. Produce ${this.fruits} fruits.`)
        } else {
            console.log(`${this.type} tree has produced ${this.totalFruits} fruits.`)
        }
    }
}

class AppleTree extends FruitTree {
    constructor() {
        super("Apple", 1, 0, 0, 5, 15);
    }
}
class PearTree extends FruitTree {
    constructor() {
        super("Pear", 2, 0, 0, 3, 10);
    }
}

let appleTree = new AppleTree();
console.log("===== APPLE TREE =====");
while (appleTree.age < appleTree.stopProducing) {
    appleTree.growUp();
    appleTree.produceFruits();
    appleTree.status();
}

let pearTree = new PearTree();
console.log("===== PEAR TREE =====");
while (pearTree.age < pearTree.stopProducing) {
    pearTree.growUp();
    pearTree.produceFruits();
    pearTree.status();
}

// Factory Class
class Garden {
    constructor(field){
        this._field = field || [];
    }
    //Getter
    get field(){
        return this._field;
    }
    //Setter
    set setField(field){
        this._field = field;
    }
    insertTree(tree){
        this._field.push(tree);
    }
    report(){
        this.field.forEach(el =>{
            console.log(`${el.type} tree has produced ${el.totalFruits} fruits.`)
        })
        console.log(this.field)
    }
}
let garden = new Garden();
console.log("===== JACK'S GARDEN=====");
garden.insertTree(appleTree);
garden.insertTree(pearTree);
garden.report();
