const armyModule = require("./army.js")
class Barrack {
    constructor(slots) {
        this._slots = slots || []
    }
    recruit(army){
        this._slots.push(army)
    }
    summon(){
        console.log("========== Mission Accomplished !!! ==========")
        return this._slots;
    }
    disband(name){
        for (let i = 0; i < this._slots.length; i ++) {
            if (this._slots[i].name == name){
                this._slots.splice(i, 1);
                console.log("Go to next Gate, now!")
            }        
         }
    }
    grouping() 
    {
        let Knight = [];
        let Spiderman = [];
        let Archer = [];
        this._slots.forEach(element => {
            switch(element.type) {
                case 'Knight' :
                    Knight.push(element);
                    break;
                case 'Spiderman' :
                    Spiderman.push(element);
                    break;
                case 'Archer' :
                    Archer.push(element);
                    break;
                default :
                    console.log('...stay away for a while from the Army!');
            }
        } );
        let obj = {Knight : Knight, Spiderman : Spiderman, Archer : Archer}
        console.log(obj);
    }
}

// console.log(Barrack.disband(name));
module.exports = Barrack;

// const armyModule = require("./army.js");
// const Army = armyModule.Army;
// const Knight = armyModule.Knight;
// const Spiderman = armyModule.Spiderman
// const Archer = armyModule.Archer

// const army = new Army("First army");
// const knight = new Knight("First knight");