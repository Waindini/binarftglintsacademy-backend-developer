const { Knight, Spiderman, Archer}= require('./army.js') //destructuring method
const barrackModule = require('./barrack.js')

const hero = new Knight ("Roberto", "Knight", 1);
const hero2 = new Spiderman ("Peter", "Spiderman", 1);
const hero3 = new Archer ("Arson", "Archer", 1);

const Barrack = new barrackModule();

Barrack.recruit(hero);
Barrack.recruit(hero2);
Barrack.recruit(hero3);

Barrack.summon();

Barrack.disband('Roberto');

Barrack.grouping();

