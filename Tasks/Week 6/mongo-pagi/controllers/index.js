const { User, Product } = require("../schemas");
const { tokenGenerator } = require("../helpers/jwt");

class UserController {
  static async viewUsers(req, res) {
    console.log("View Users");
    try {
      const users = await User.find({}).sort({ createdAt: -1 }).limit(5);

      res.status(200).json(users);
    } catch (err) {
      res.status(500).json({
        message: err,
      });
    }
  }
  static async login(req, res) {
    const { username, password } = req.body;
    console.log("Login Users");
    try {
      const user = await User.findOne({ username: username });

      console.log(user, "--user");

      if (user.password === password) {
        const access_token = tokenGenerator(user);
        res.status(200).json({ token: access_token });
      } else {
        throw {
          status: 404,
          msg: "User is not found.",
        };
      }
    } catch (err) {
      res.status(500).json({
        message: err,
      });
    }
  }
  static async register(req, res) {
    const { username, password } = req.body;
    console.log("Register Users");
    try {
      const user = await User.create({
        username,
        password,
      });

      res.status(201).json(user);
    } catch (err) {
      res.status(500).json({
        message: err,
      });
    }
  }
}

class ProductController {
  // static async productById(req, res){
  //     const id = req.params.userId
  //     try{
  //         const user = await Product.findById(id)
  //         res.status(200).json(user)
  //     }catch(err){
  //         res.status(500).json({
  //             message : err
  //         })
  //     }
  // }
}

module.exports = {
  UserController,
  ProductController,
};
