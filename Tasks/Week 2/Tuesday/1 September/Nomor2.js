//Exercise Number 2
//2. Print the n first numbers//
//Test Case

//Input 15
//answer

var n = 15;

for(let i=1 ; i<n; i++){
    if(i % 3 === 0 && i % 5 === 0){
        console.log(" kelipatan 3 dan 5");
    }
    else if(i % 5 === 0){
        console.log(" kelipatam 5");
    }
    else if(i % 3 === 0){
        console.log(" kelipatan 3");
    }
    else{
        console.log(i);
    }
}