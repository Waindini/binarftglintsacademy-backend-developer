//Exercise Nomor 1
//1. Check odd or even number//
//javascript
    //Test Case
    //Input 5 -> Output Odd
    //Input 10 -> Output Even
//answer
//Odd Number is integer that cannot divisible by 2
//Even Number is integer that can divisible by 2

var isEven = function(number) {
    if (number % 2 === 0){
        console.log(number);  
    }
    else if (number % 2 !== 0) {
        console.log(number);
    }
    else{
        return true;
    }
}
//console.log(number);
