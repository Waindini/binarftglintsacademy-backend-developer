// index.js
const armyModule = require("./army.js");
const Army = armyModule.Army;
const Knight = armyModule.Knight;

const army = new Army("First army");
const knight = new Knight("First knight");

console.log(army.getName());
console.log(knight.getName());
knight.attack();

// Relative path:
// Relative dengan level nya file yang memanggil
// contoh relative path ./army.js
