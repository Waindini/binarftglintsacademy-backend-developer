// Import class class yang dibutuhkan
const Dog = require("./classes/Dog");
const Cat = require("./classes/Cat");

// class yang akan membuat instance result
class Result {
  constructor() {
    this.dog = []; // array yang akan diisi dari instance Dog
    this.cat = []; // array yang akan diisi dari instance Cat
  }

  // method untuk mengisi this.dog dengan instance Dog
  addDog(dog) {
    this.dog.push(new Dog("dog", dog));
  }

  // method untuk mengisi this.cat dengan instance Cat
  addCat(cat) {
    this.cat.push(new Cat("cat", cat));
  }

  // method untuk mereturn array this.dog dan array this.cat
  getData() {
    return {
      dog: this.dog,
      cat: this.cat
    };
  }
}

const result = new Result();

// mengisi result dengan 3 dog berdasarkan habitat nya
const dogHabitats = ["kintamani", "makassar", "jakarta"];
for (var i = 0; i < dogHabitats.length; i++) {
  // loop semua habitat
  result.addDog(dogHabitats[i]); // isi array dog
}

// mengisi result dengan 3 cat berdasarkan habitat nya
const catHabitats = ["hutan", "kota", "desa"];
for (var i = 0; i < catHabitats.length; i++) {
  // loop semua habitat
  result.addCat(catHabitats[i]); // isi array cat
}

// return array this.dog sama array this.cat
const data = result.getData();
/*
  data:
  {
    dog: [
      {
        species: "dog",
        habitat: "kintamani"
      },
      ...
    ],
    cat: [
      {
        species: "cat",
        habitat: "hutan"
      },
      ...
    ]
  }
*/

console.log("-----------------DOG-----------------------");
for (var i = 0; i < data.dog.length; i++) {
  // loop semua dog didalam array this.dog
  if (data.dog[i].habitat === "makassar") {
    // kalau habitat dog dari makassar, maka panggil method dog.bark()
    console.log("anjing dari makassar mau guk guk..");
    data.dog[i].bark();
    data.dog[i].sayMySpecies();
  }
}
console.log("-----------------CAT-----------------------");
for (var i = 0; i < data.cat.length; i++) {
  // loop semua cat didalam array this.cat
  data.cat[i].miauw(); // meong meong...
  data.cat[i].sayMySpecies();
}
