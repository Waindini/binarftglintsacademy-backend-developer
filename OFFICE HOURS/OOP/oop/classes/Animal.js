/*
 Animal
 @desc:
 - class utama
 @fields:
 - species: string
 - habitat: string
 @methods:
 - sayMySpecies => log
*/
class Animal {
  constructor(species, habitat) {
    this.species = species;
    this.habitat = habitat;
  }

  sayMySpecies() {
    console.log("My species is", this.species);
  }
}

module.exports = Animal;
