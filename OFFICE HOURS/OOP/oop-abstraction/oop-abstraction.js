// Abstraction
class Person {
  constructor(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }
  
  // public method biasa
  setName(fN) {
    console.log("setting full name using setName...");
    const [firstName, lastName] = fN.split(" ");
    this.firstName = firstName;
    this.lastName = lastName;
  }

  // encapsulations
  // private method
  _setFullName(fN) {
    console.log("setting full name using setFullName...");
    const [firstName, lastName] = fN.split(" ");
    this.firstName = firstName;
    this.lastName = lastName;
  }

  get fullName() {
    console.log("returning full name..");
    console.log("ada validasi mungkin...");
    return this.firstName + " " + this.lastName;
  }

  set fullName(fN) {
    console.log("setting full name..");
    try {
      if (!fN) {
        throw "value must be not empty";
      }

      if (fN.split(" ").length < 2) {
        throw "value must full name";
      }

      this._setFullName(fN);
    } catch (err) {
      console.error(err);
    }
  }
  // end of encapsulations
}

// Inheritance
class Agus extends Person {
  sayHello() {
    console.log("hello from", this.fullName);
  }
}
// end of Inheritance
// end of Abstraction

const person = new Person("John", "Doe");

person.setName("Jane Doe");
console.log(person.firstName, person.lastName);

var fullName = person.fullName;
console.log(fullName);

person.fullName = "Nasser Maronie";
console.log(person.fullName);

const agus = new Agus("Agus", "Priyatin");
agus.sayHello();
