// Buatlah sebuah Class Student, yang memiliki atribut berikut:
// Name, Age, Date of Birth, Gender, Student ID (bisa berupa angka atau teks), dan Hobbies (bisa menampung lebih dari 1 hobi).
// Class tersebut juga bisa memanggil fungsi dengan proses sebagai berikut:

// SetName: mengubah nama student dengan mengirimkan satu parameter ke dalam fungsi berupa teks
// ● SetAge: mengubah umur student dengan mengirimkan satu parameter ke dalam fungsi berupa angka
// ● SetDateOfBirth: mengubah tanggal lahir student dengan mengirimkan satu parameter ke dalam fungsi berupa teks
// ● SetGender: mengubah gender student dengan mengirimkan satu parameter ke dalam fungsi berupa teks, dan hanya
// bisa menerima nilai Male atau Female
// ● addHobby: menambah hobi dengan mengirimkan satu parameter ke dalam fungsi berupa teks
// ● removeHobby: menghapus list hobi yang ada dengan mengirimkan satu parameter berupa teks, yang merupakan hobi
// apa yang akan dihapus
// ● getData: menampilkan seluruh data atribut murid

// answer
class ClassStudents {
  constructor(Name, Age, dateOfbirth, Gender, studentId) {
    this._name = Name;
    this._age = Age;
    this._DateOfBirth = dateOfbirth;
    this._gender = Gender;
    this._studentsID = studentId;
    this._Hobby = ["Travelling", "Cooking", "Reading"];
   // this._obj = ""; //abstraction tuh gini gaksih? seemslike we've another new propeerty
  }

  //pasang dulu bosq getter setter
  get getData() {
    console.log(this);
  }

  //expl setter
  set setNameAndAge(newName) {
    this._name = newName;
  }

//inheritance
const person = new ClassStudents(
  "Dini",
  24,
  "enam oktober 1996",
  "female",
  007202000200);
console.log(person);
}
//person.Hobby();
// person.setName = console.log(person, "---person");

// let arr = ["Hobby1", "Hobby2", "Hobby3"];

// let result = ["Hobby1", "Hobby2"];

// // 1. cari index elemen yang mau didelete di array
// // 2. delete elemennya menggunakan index itu
// // 3. return arraynya

// // googling
// // how to search index element in array javascript || indexof
// // how to remove element in array by index javascript || splice/slice

// let index = arr.indexOf("Hobby3");
// arr.splice(index, 1);

// console.log(index, "--index");

// console.log(arr, "--array");
