// Array destructuring
// rest operator
let [a, b, ...c] = [1, 2, 3, 4, 5, 6, 7, 8];

let { name, age } = { name: "Fatah", age: 16 };

// console.log({
//   a: a,
//   b: b,
//   c: c.join(","),
// });

let arr = [1, 2, 3, 4, 5, 6, 7];

let newArr = [10, ...arr, 20];

// console.log(newArr, "--new arr");

// OOP - Encapsulation
class Student {
  constructor(name, age, gender, passwords, atmCard) {
    this.name = name;
    this.age = age;
    this.gender = gender; // public
    const school = "Glints Academy"; // private
    const password = passwords;
    const _semester = 2; // protected
    const atmCardNumber = atmCard;
    this.getAtmCard = function () {
      return atmCardNumber;
    };
    this.forgetPassword = function () {
      return password;
    };
  }

  static getSchool() {
    // Parent's method, instance won't get it
    return "Glints Academy";
  }
}

let fatah = new Student("Fatah", 17, "Male", "fatah123", 12345678);
// console.log(fatah.getAtmCard(), "--fatah");

// console.log(Student.getSchool(), "--get skul");

// OOP - Abstraction
class Ongkir {
  constructor(berat) {
    this.berat = berat;
    this.biaya = 5000; // price per kg
    const pajak = 0.1;
    this.hitungTotalBiaya = () => {
      return this.berat * this.biaya * pajak;
    };
    this.getBiaya = () => {
      return biaya;
    };
  }

  get Biaya() {
    return this._biaya;
  }

  set aturBiaya(newBiaya) {
    this._biaya = newBiaya;
  }
}

let ongkir1 = new Ongkir(5);

console.log(ongkir1.biaya, "--ongkir 1");
