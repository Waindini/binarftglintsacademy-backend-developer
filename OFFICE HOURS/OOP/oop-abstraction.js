class Person {
  constructor(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  // ini bukan abstraction, cuman method biasa
  setFullName(fN) {
    console.log("setting full name using setFullName...");
    const [firstName, lastName] = fN.split(" ");
    this.firstName = firstName;
    this.lastName = lastName;
  }

  // abstraction
  get fullName() {
    console.log("returning full name..");
    console.log("ada validasi mungkin...");
    return this.firstName + " " + this.lastName;
  }

  set fullName(fN) {
    console.log("setting full name..");
    try {
      if (!fN) {
        throw "value must be not empty";
      }

      if (fN.split(" ").length < 2) {
        throw "value must full name";
      }

      const [firstName, lastName] = fN.split(" ");
      this.firstName = firstName;
      this.lastName = lastName;
    } catch (err) {
      console.error(err);
    }
  }
  // end of abstraction
}

const person = new Person("Agus", "Priyatin");

// Ini bukan abstraction
person.setFullName("John Doe");
console.log(person.firstName, person.lastName);

// abstraction
var fullName = person.fullName;
console.log(fullName);

person.fullName = "Nasser Maronie";
console.log(person.fullName);
